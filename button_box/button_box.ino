#include <SD.h> // need to include the SD library
#define SD_ChipSelectPin 8 //connect pin 4 of arduino to cs pin of sd card
#include <TMRpcm.h> //Arduino library for asynchronous playback of PCM/WAV files
#include <SPI.h> //  need to include the SPI library
#include <MFRC522.h>

const int buttonPin = 2;

int buttonState = 0;   
int oldButtonState = 0;
long lastClick = 0;
long clickDelay = 300;

File playingDir;
String pathDir = "/";

TMRpcm tmrpcm; // create an object for use in this sketch

void setup()
{ 
  
  Serial.begin(9600);
  SPI.begin(); // init SPI bus
 
 tmrpcm.speakerPin = 9; //5,6,11 or 46 on Mega, 9 on Uno, Nano, etc

 if (!SD.begin(SD_ChipSelectPin)) // returns 1 if the card is present
 {
  Serial.println("SD fail");
  return;
 }

 tmrpcm.setVolume(1);

 // initialize the pushbutton pin as an input:
 pinMode(buttonPin, INPUT);
 Serial.println("ready to play");
  
 playingDir = SD.open(pathDir);
 printDirectory(playingDir, 0);
 
                     
}


void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  if (!tmrpcm.isPlaying()) {
    playNextFile();
  }

 // prevent flickers
  if (lastClick < (millis() - clickDelay)) {
     // check if the pushbutton is pressed
    if (buttonState == HIGH && oldButtonState == LOW) {
      // pause current song
      tmrpcm.pause();
      // play song
      oldButtonState = HIGH;

      playNextFile();

      lastClick = millis();
    } else if (buttonState == LOW && oldButtonState == HIGH) {
      // reset state
      oldButtonState = LOW;
    }
  }

}

String playNextFile() {
  File entry = playingDir.openNextFile();
  if (! entry || entry.isDirectory()) {
      entry.close();
      playingDir = SD.open(pathDir);
      entry = playingDir.openNextFile(); 
        }
  String fileName = entry.name();      
  Serial.println("play " + fileName);
  tmrpcm.play(fileName.c_str());
  return fileName;      
}


// https://reference.arduino.cc/reference/en/libraries/sd/opennextfile/
void printDirectory(File dir, int numTabs) {
  while (true) {
    File entry = dir.openNextFile();
    if (!entry) {
      if (numTabs == 0)
        Serial.println("** Done **");
      return;
    }

    for (uint8_t i = 0; i < numTabs; i++)
      Serial.print('\t');

    Serial.print(entry.name());

    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }

    entry.close();
  }
}
